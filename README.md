# Jest ReportPortal Reporter
ReportPortal integration for Jest based on [Javascript ReportPortal client](https://github.com/reportportal/client-javascript)

## Installation
`npm install -S`

## Usage
Configure Jest to process the test results by adding the following entry to the Jest config:
```
"reporters": [
	"default",
	["jest-rp-reporter"]
]
```
To send results to ReportPortal you need to provide following env vars:

* REPORT_PORTAL_TOKEN=
* REPORT_PORTAL_URL=
* REPORT_PORTAL_PROJECT=
* REPORT_PORTAL_LAUNCH_NAME=

If `BUILD_NUMBER` env var provided, than it will add description `build #BUILD_NUMBER` to launch description.

If `LOG_COMMUNICATION=true` provided it will try to append log file from `log` folder to failed test item.

It adds `ENVIRONMENT` value into tags automatically.


## Licence
This project is licensed under the terms of the [MIT license](LICENSE.md).