const RPClient = require('reportportal-client');
const fs = require('fs');
const path = require('path');
const shouldLogFiles = process.env.LOG_COMMUNICATION === 'true';

const DEFAULT_LOG_DIR = 'logs';
const DEFAULT_TEST_DIR = 'tests';

class ReportPortalReporter {
  constructor(globalConfig, options) {
    this._globalConfig = globalConfig;
    this._options = options;
    this.launchObj = {};
    this.logDir = options.logDir ? options.logDir : DEFAULT_LOG_DIR;
    this.testDir = options.testDir ? options.testDir : DEFAULT_TEST_DIR;
    this.rpClient = new RPClient({
      token: process.env.REPORT_PORTAL_TOKEN,
      endpoint: `${process.env.REPORT_PORTAL_URL}/api/v1`,
      project: process.env.REPORT_PORTAL_PROJECT,
      launch: "LAUNCH_NAME"
    });
  }
  async onRunStart(context) {

    this.rpClient.checkConnect().then((response) => {
      console.log('You have successfully connected to the server.');
      console.log(`You are using an account: ${response.full_name}`);
    }, (error) => {
      console.log('Error connection to server');
      console.dir(error);
    });


    this.launchObj = await this.rpClient.startLaunch({
      name: process.env.REPORT_PORTAL_LAUNCH_NAME || 'Test',
      start_time: this.rpClient.helpers.now(),
      tags: [process.env.ENVIRONMENT],
      description: process.env.BUILD_NUMBER ? `build #${process.env.BUILD_NUMBER}` : ''
    });
  }

  onTestResult(context, result) {
    let suites = [];
    result.testResults.forEach((_, idx) => {
      // suites
      for (let i = 0; i < _.ancestorTitles.length; i++) {
        const suiteName = _.ancestorTitles[i];
        const suiteAlreadyStarted = suites.find(_ => _.name === suiteName);
        if (!suiteAlreadyStarted) {
          // suite with parent
          if (i > 0) {
            const parentSuite = suites.find(s => s.name === _.ancestorTitles[i - 1]);
            const suiteObj = this.startSuite(suiteName, parentSuite.tempId);
            suites.push({ tempId: suiteObj.tempId, name: suiteName });
          }
          // first suite
          else {
            const suitObj = this.startSuite(suiteName);
            suites.push({ tempId: suitObj.tempId, name: suiteName });
          }
        }

        // last item == test step
        if (i === _.ancestorTitles.length - 1) {
          const currentSuite = suites.find(_ => _.name === suiteName)
          const stepObj = this.startStep(_.title, currentSuite.tempId);
          this.finishTestItem(stepObj.tempId, _.status);
          if (_.status === 'failed') {
            this.sendLogWithFile(stepObj.tempId, this.removeAnsi(_.failureMessages.toString()), result.testFilePath);
          }
          // if not last item and suites not empty
          if (idx < result.testResults.length - 1 && suites.length > 0) {
            // if current describe block does not match the next one, than finish it
            const nextSuite = result.testResults[idx + 1].ancestorTitles[i];
            if (suiteName !== nextSuite && nextSuite) {
              this.finishTestItem(currentSuite.tempId, _.status);
              suites = suites.filter(_ => _.name != currentSuite.name);
            }
          }
        }
      }

    })
    // finish all suites from last to first
    const suitesBackwards = suites.slice(0).reverse();
    suitesBackwards.forEach(_ => {
      this.finishTestItem(_.tempId, 'passed')
    })
  }

  // finish launch
  onRunComplete(contexts, results) {
    this.rpClient.finishLaunch(this.launchObj.tempId, {
      end_time: this.rpClient.helpers.now()
    });
  }

  startSuite(suiteName, parentId = undefined) {
    return this.rpClient.startTestItem({
      description: '',
      name: suiteName,
      start_time: this.rpClient.helpers.now(),
      type: "SUITE"
    }, this.launchObj.tempId, parentId);
  }

  startStep(stepName, parentId) {
    return this.rpClient.startTestItem({
      description: '',
      name: stepName.length > 256
        ? stepName.substring(0, 255)
        : stepName,
      start_time: this.rpClient.helpers.now(),
      type: "STEP"
    }, this.launchObj.tempId, parentId);
  }

  finishTestItem(itemId, status = undefined) {
    // changing pending status to skipped
    status = status === 'pending' ? 'skipped' : status
    return this.rpClient.finishTestItem(itemId, { status });
  }

  sendLogWithFile(itemId, message, fileName) {
    const dataObj = {
      level: "ERROR",
      message,
      time: this.rpClient.helpers.now()
    };
    const parsedFileName = path.parse(fileName);
    const relativeTestDirPath = this.getRelativeTestPath(parsedFileName.dir, this.testDir);
    const strippedFilename = parsedFileName.name.split('.')[0];
    const latestLogFile = this.findLatestLog(`./${this.logDir}${relativeTestDirPath}`, strippedFilename);
    if (shouldLogFiles && latestLogFile) {
      return this.rpClient.sendLog(itemId, dataObj, {
        name: `errorFile`,
        type: 'text/plain',
        content: fs.readFileSync(`./${this.logDir}${relativeTestDirPath}/${latestLogFile}`, { encoding: "base64" })
      });
    } else {
      // sending log without file
      return this.rpClient.sendLog(itemId, dataObj);
    }
  }

  removeAnsi(s) {
    const text = s.replace(
      /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, '');
    return text
  }


  findLatestLog(dir, fileName) {
    const files = fs.readdirSync(dir)
    const latest = files.map(_path => ({ stat: fs.lstatSync(path.join(dir, _path)), dir: _path }))
      .filter(_path => _path.stat.isFile())
      .filter(_ => _.dir.includes(fileName))
      .sort((a, b) => b.stat.mtime - a.stat.mtime)
      .map(_path => _path.dir);
    return latest[0];
  }

  getRelativeTestPath(path, testsDir) {
    const regExp = `\/${testsDir}`;
    const pathBaseRegExp = new RegExp(regExp);
    const pathBaseIdx = path.search(pathBaseRegExp);
    return path.substr(pathBaseIdx + regExp.length);
  }
}

module.exports = ReportPortalReporter;
